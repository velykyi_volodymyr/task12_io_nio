package com.velykyi.model.ClientServer;


import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class Server {
    private static Selector selector;

    public static void main(String[] args) {
        System.out.println("Server starting...");
        try {
            InetAddress hostIP = InetAddress.getLocalHost();
            int port = 9090;
            System.out.printf("Trying to accept connection on %s:%d\n", hostIP.getHostAddress(), port);
            selector  = Selector.open();
            ServerSocketChannel socketChannel = ServerSocketChannel.open();
            ServerSocket socket = socketChannel.socket();
            InetSocketAddress address = new InetSocketAddress(hostIP, port);
            socket.bind(address);
            socketChannel.configureBlocking(false);
            int ops = socketChannel.validOps();
            socketChannel.register(selector, ops, null);
            while (true) {
                selector.select();
                Iterator<SelectionKey> i = selector.selectedKeys().iterator();
                while (i.hasNext()) {
                    SelectionKey key = i.next();
                    if (key.isAcceptable()) {
                        acceptEvent(socketChannel, key);
                    } else if( key.isReadable()) {
                        readEvent(key);
                    }
                    i.remove();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readEvent(SelectionKey key) throws IOException {
        System.out.println("Reading...");
        SocketChannel client = (SocketChannel) key.channel();
        ByteBuffer buf = ByteBuffer.allocate(1024);
        client.read(buf);
        String data = new String(buf.array()).trim();
        if (data.length() > 0) {
            if (data.equalsIgnoreCase("q")) {
                client.close();
                System.out.println("Server connection closing...");
            } else {
                System.out.printf("Message: %s\n", data);
            }
        }
    }

    private static void acceptEvent(ServerSocketChannel socketChannel, SelectionKey key) throws IOException {
        System.out.println("Connection accepted...");
        SocketChannel client = socketChannel.accept();
        client.configureBlocking(false);
        client.register(selector, SelectionKey.OP_READ);
    }

}
