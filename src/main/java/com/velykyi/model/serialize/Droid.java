package com.velykyi.model.serialize;

import java.io.Serializable;

public class Droid implements Serializable {
    private String model;
    private int id;
    private transient String comment = "No comment.";

    public Droid(String model, int id, String comment) {
        this.model = model;
        this.id = id;
        this.comment = comment;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String toString() {
        return "Model: " + this.model + "\nId: " + this.id + "\nComment:  " + this.comment;
    }
}
