package com.velykyi.model.readAndWrite;

import org.apache.commons.lang3.time.StopWatch;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Usual {

    public static void write(String filename, List<Integer> dataList) {
        StopWatch stopWatch = new StopWatch();
        int count = 0;
        try{
            OutputStream out = new FileOutputStream(filename);
            stopWatch.start();
            for (int data : dataList) {
                out.write(data);
            }
            stopWatch.stop();
            out.close();
            System.out.println(stopWatch.getTime() + " milliseconds");
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Integer> read(String filename) {
        StopWatch stopWatch = new StopWatch();
        int count = 0;
        List<Integer> data = new ArrayList<>();
        try {
            InputStream in = new FileInputStream(filename);
            stopWatch.start();
            do {
                data.add(in.read());
                count++;
//                System.out.println(data);
            } while (data.get(count - 1)!= -1 );
            stopWatch.stop();
            System.out.println(stopWatch.getTime() + " milliseconds");
            in.close();
        } catch (FileNotFoundException e){
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }


}
