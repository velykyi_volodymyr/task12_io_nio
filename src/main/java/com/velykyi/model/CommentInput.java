package com.velykyi.model;

import org.apache.commons.lang3.time.StopWatch;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CommentInput {

    private static List<String> data;

    public static List<String> takeCode(String filename){
        data = new ArrayList<>();
        int count = 0;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            do {
                data.add(in.readLine());
                count++;
            } while (data.get(count - 1) != null);
            data.remove(null);
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static List<String> takeComment() {
        List<String> commentList = new ArrayList<>();
        for (String item : data) {
            if (item.startsWith("//")) {
                commentList.add(item);
            }
        }
        return commentList;
    }
}
