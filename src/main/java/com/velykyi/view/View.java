package com.velykyi.view;

import com.velykyi.controller.Controller;
import com.velykyi.model.readAndWrite.Buffer;
import com.velykyi.model.readAndWrite.Usual;
import com.velykyi.model.serialize.Droid;
import com.velykyi.model.serialize.Ship;

import java.util.*;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Controller controller = new Controller();
    private static Scanner scann = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("q", bundle.getString("q"));
    }

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::serialize);
        methodMenu.put("2", this::usualReadWrite);
        methodMenu.put("3", this::bufferReadWrite);
        methodMenu.put("4", this::getCodeComments);
        methodMenu.put("5", this::getCurrentDir);
        methodMenu.put("6", this::getDir);
        methodMenu.put("7", this::setEnglish);
        methodMenu.put("8", this::setUkrainian);
        methodMenu.put("9", this::setGerman);
        methodMenu.put("10", this::setGreek);
    }

    public void showMenu() {
        String keyMenu;
        do {
            System.out.println("\nMENU:");
            for (String str : menu.values()) {
                System.out.println(str);
            }
            System.out.println("Please, select menu point.");
            keyMenu = scann.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.toUpperCase().equals("Q"));
    }

    private void setEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setUkrainian(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setGerman(){
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setGreek(){
        locale = new Locale("el");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void getCurrentDir() {
        controller.getCurrentDir();
    }

    private void getDir() {
        System.out.println("Enter your directory(ex. D:\\Курсова 2019)");
        controller.getDir(scann.nextLine());
    }

    private void getCodeComments() {
        controller.getCodeComments();
    }

    private void bufferReadWrite() {
        controller.bufferReadWrite();
    }

    private void usualReadWrite() {
        controller.usualReadWrite();
    }

    private void serialize() {
        controller.serialize();
    }


}
