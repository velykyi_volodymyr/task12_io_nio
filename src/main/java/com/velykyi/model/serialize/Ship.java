package com.velykyi.model.serialize;


import java.io.*;

public class Ship implements Serializable {
    private int amount;
    private Droid droid;

    public Ship(int amount, Droid droid) {
        this.amount = amount;
        this.droid = droid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Droid getDroid() {
        return droid;
    }

    public void setDroid(Droid droid) {
        this.droid = droid;
    }

    public String toString() {
        return this.droid.toString() + "\nAmount:  " + this.amount;
    }

    public void serialize(String filename) {
        try (FileOutputStream file = new FileOutputStream(filename)){
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(this);
            out.close();
            System.out.println("Serialized data ...");
            System.out.println(this);
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.out.println("Name of file is null!");
            e.printStackTrace();
        }
    }

    public void deserialize(String filename, Ship newShip) {
        try (FileInputStream file = new FileInputStream(filename)){
            ObjectInputStream in = new ObjectInputStream(file);
            newShip = (Ship) in.readObject();
            in.close();
            System.out.println("Deserialized Ship ...");
            System.out.println(newShip);
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class does not exist.");
            e.printStackTrace();
        }
    }
}
