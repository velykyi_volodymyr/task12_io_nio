package com.velykyi.model.readAndWrite;

import org.apache.commons.lang3.time.StopWatch;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Buffer {

    public static void write(String filename, List<Byte> dataList) {
        StopWatch stopWatch = new StopWatch();
        int count = 0;
        try{
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filename)));
            stopWatch.start();
            for (byte data : dataList) {
                out.write(data);
            }
            stopWatch.stop();
            out.close();
            System.out.println(stopWatch.getTime() + " milliseconds");
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Byte> read(String filename) {
        int count = 0;
        List<Byte> data = new ArrayList<>();
        StopWatch stopWatch = new StopWatch();
        try {
            DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)));
            stopWatch.start();
            do {
                data.add(in.readByte());
                count++;
            } while (data.get(count - 1) != -1);
            stopWatch.stop();
            System.out.println(stopWatch.getTime() + " milliseconds");
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static List<Byte> read(String filename, int bufferSize) {
        int count = 0;
        List<Byte> data = new ArrayList<>();
        StopWatch stopWatch = new StopWatch();
        try {
            DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(filename), bufferSize));
            stopWatch.start();
            do {
                data.add(in.readByte());
                count++;
            } while (data.get(count - 1) != -1);
            stopWatch.stop();
            System.out.println(stopWatch.getTime() + " milliseconds");
            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
