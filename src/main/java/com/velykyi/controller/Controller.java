package com.velykyi.controller;

import com.velykyi.model.CommentInput;
import com.velykyi.model.Directory;
import com.velykyi.model.readAndWrite.Buffer;
import com.velykyi.model.readAndWrite.Usual;
import com.velykyi.model.serialize.Droid;
import com.velykyi.model.serialize.Ship;

import java.util.List;

public class Controller {

    public void bufferReadWrite() {
        Buffer.write("Buffer_write", Buffer.read("Брюс Эккель - Философия Java - 2015.pdf"));
        for (int i = 1; i < 100;  i = i+10) {
            System.out.println(1024*i);
            Buffer.read("Брюс Эккель - Философия Java - 2015.pdf", 1024*i);
        }
    }

    public void usualReadWrite() {
        Usual.read("Брюс Эккель - Философия Java - 2015.pdf");
        Usual.write("Usual_write.txt", Usual.read("Ship.dat"));
    }

    public void serialize() {
        String file = "Ship.dat";
        Ship ship = new Ship(5, new Droid("R2D2", 1, "Track droid."));
        ship.serialize(file);
        Ship newShip = null;
        ship.deserialize(file, newShip);
    }

    public void getCodeComments() {
        List<String> data = CommentInput.takeCode("D:\\Epam Java Course\\Topic 14. IO , NIO\\task12_io_nio\\src\\main\\java\\com\\velykyi\\App.java");
        System.out.println("Code: ");
        for (String item : data) {
            System.out.println(item);
        }

        List<String> commentList = CommentInput.takeComment();
        System.out.println("Comments: ");
        for (String item : commentList) {
            System.out.println(item);
        }
    }

    public void getCurrentDir() {
        Directory.showCurrent();
    }

    public void getDir(String dir) {
        Directory.show(dir);
    }
}
