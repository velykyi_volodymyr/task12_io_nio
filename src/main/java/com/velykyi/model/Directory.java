package com.velykyi.model;

import java.io.File;

public class Directory {

    public static void showCurrent() {
        System.out.println("Current directory: ");
        File file = new File(".");
        if (file.isDirectory()) {
            printDirectory(file, "");
        }
    }

    public static void show(String dir) {
        File file = new File(dir);
        if (file.isDirectory()) {
            printDirectory(file, "");
        }
    }

    private static void printDirectory(File dir, String s) {
        System.out.println(s + "Directory: " + dir.getName());
        s = s + " ";
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                printDirectory(file, s);
            } else {
                System.out.println(s + "File: " + file.getName());
            }
        }
    }
}
