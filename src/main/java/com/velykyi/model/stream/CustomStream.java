package com.velykyi.model.stream;

import java.io.*;

public class CustomStream{
    InputStream in;
    OutputStream out;


    public CustomStream(String filename) throws FileNotFoundException {
        this.in = new BufferedInputStream(new FileInputStream(filename));
        this.out = new BufferedOutputStream(new FileOutputStream(filename));
    }

    public int read() throws IOException {
        return in.read();
    }

    public void write(int data) throws IOException {
        out.write(data);
    }

    public byte[] read(byte[] data) throws IOException {
        int c;
        if (data == null) {
            throw new NullPointerException();
        }
        for(byte item : data) {
            c = read();
            if(c == -1) {
                break;
            } else {
                item = (byte)c;
            }
        }
        return data;
    }

    public void write(byte[] data) throws IOException {
        for (byte item : data) {
            write(item);
        }
    }
}
