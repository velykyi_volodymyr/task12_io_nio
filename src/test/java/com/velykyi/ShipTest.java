package com.velykyi;

import com.velykyi.model.serialize.Droid;
import com.velykyi.model.serialize.Ship;
import org.junit.Test;

import java.io.*;

public class ShipTest {

    @Test(expected = NullPointerException.class)
    public void testSerialize() {
        Ship ship = new Ship(5, new Droid("R2D2", 1, "Track droid."));
        ship.serialize(null);
    }

    @Test(expected = FileNotFoundException.class)
    public void testDeserialize() {
        String file = "Ship.dat";
        Ship ship = new Ship(5, new Droid("R2D2",1,"Track droid."));
        ship.serialize(file);
        Ship newShip = null;
        ship.deserialize("file.dat", newShip);
    }
}
