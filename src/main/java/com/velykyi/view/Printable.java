package com.velykyi.view;

@FunctionalInterface
public interface Printable {
    void print();
}
