package com.velykyi.model.ClientServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

public class Client {

    static Scanner scann = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Client starting...");
        try{
            String message;
            int port = 9090;
            InetAddress hostIP = InetAddress.getLocalHost();
            InetSocketAddress address = new InetSocketAddress(hostIP, port);
            SocketChannel client = SocketChannel.open(address);
            System.out.printf("Trying to connect to %s:%d\n", hostIP.getHostAddress(), port);
            do {
                message = scann.nextLine();
                ByteBuffer buf = ByteBuffer.allocate(1024);
                buf.put(message.getBytes());
                buf.flip();
                client.write(buf);
            } while(!message.toLowerCase().equals("q"));

            System.out.println("Client connect closing...");
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
