package com.velykyi;

import com.velykyi.model.readAndWrite.Channel;
import com.velykyi.view.View;

import java.io.IOException;

public class App {
    public static void main(String[] args) {
        View view = new View();
        view.showMenu();
    }
}
